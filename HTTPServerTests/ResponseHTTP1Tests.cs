﻿using Xunit;
using SimpleHTTPServer;
using SimpleHTTPServer.Models;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace HTTPServerTests
{
    public class ResponseHTTP1Tests
    {
        [Fact]
        public void While_file_does_not_exists_return_not_found_response()
        {
            var data = new HTTPData(Properties.Resources.HttpRequestFixture);
            var sut = new ResponseHTTP1GetHead("C://invalid//path.html", data, StatusCode.OK);            
            var expectedResponse= HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.NotFound,
                new Dictionary<string, string>(), string.Empty);

            var response = sut.ToBytes();

            Assert.Equal(StatusCode.NotFound, sut.StatusCode);
            Assert.Equal(expectedResponse, response);
        }

        [Fact]
        public void Response_with_text_type_file_is_correct()
        {
            var htmlFixturePath = System.AppDomain.CurrentDomain.BaseDirectory +
                "/../../../Properties/siteFixture.html";
            var htmlFile = File.ReadAllText(htmlFixturePath);
            var headers = new Dictionary<string, string>
            {
                { "Content-Type", "text/html; charset=UTF-8" },
                { "Content-Length", htmlFile.Length.ToString() }
            };
            var data = new HTTPData(Properties.Resources.HttpRequestFixture);
            var sut = new ResponseHTTP1GetHead(htmlFixturePath, data, StatusCode.OK);
            var expectedResponse = HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.OK,
                headers, htmlFile);

            var response = sut.ToBytes();

            Assert.Equal(expectedResponse, response);
        }

        [Fact]
        public void Response_with_binary_type_file_is_correct()
        {
            var picturePath = System.AppDomain.CurrentDomain.BaseDirectory +
                "/../../../Properties/test.png";
            var imgFile = File.ReadAllBytes(picturePath);
            var headers = new Dictionary<string, string>
            {
                { "Content-Type", "image/png" },
                { "Content-Length", imgFile.Length.ToString() },
                { "Accept-Range", "bytes" }
            };
            var data = new HTTPData(Properties.Resources.HttpRequestFixture);
            var sut = new ResponseHTTP1GetHead(picturePath, data, StatusCode.OK);
            var expectedResponse = HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.OK,
                headers, imgFile);

            var response = sut.ToBytes();

            Assert.Equal(expectedResponse, response);
        }

        [Fact]
        public void Response_with_partial_binary_type_file_is_correct()
        {
            string picturePath = System.AppDomain.CurrentDomain.BaseDirectory +
                "/../../../Properties/test.png";
            var imgFile = File.ReadAllBytes(picturePath);
            var headers = new Dictionary<string, string>
            {
                { "Content-Type", "image/png" },
                { "Content-Length", "2" },
                { "Accept-Range", "bytes" },
                { "Content-Range", $"bytes 0-1/{imgFile.Length}" }
            };
            var data = new HTTPData(Properties.Resources.HttpRequestFixtureWithRange);
            var sut = new ResponseHTTP1GetHead(picturePath, data, StatusCode.OK);
            var expectedResponse = HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.PartialContent,
                headers, new byte[] { imgFile[0], imgFile[1] });

            var response = sut.ToBytes();

            var test = Encoding.UTF8.GetString(response);

            Assert.Equal(expectedResponse, response);
        }
    }
}
