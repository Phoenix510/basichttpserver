﻿using SimpleHTTPServer.Models;
using SimpleHTTPServer.Validation;
using System.Collections.Generic;
using Xunit;

namespace HTTPServerTests
{
    public class ValidationTests
    {
        [Theory]
        [MemberData(nameof(MethodData.Data), MemberType = typeof(MethodData))]
        public void MethodIsSetCorrectly(string method, RequestMethod expectedResult)
        {
            var result = method.ToRequestMethod();
            Assert.Equal(expectedResult, result);
        }
       
        [Theory]
        [InlineData("/", true)]
        [InlineData("/test", true)]
        [InlineData("/test.html", true)]
        [InlineData("/../../config.txt", false)]
        [InlineData("/........./../config.txt", false)]
        public void PathIsValidatedCorrectly(string path, bool expectedResult)
        {
            bool result = Validator.ValidatePath(path);
            Assert.Equal(expectedResult, result);
        }
    }

    public class MethodData
    {
        public static IEnumerable<object[]> Data =>
            new List<object[]>
            {
            new object[] { "get", RequestMethod.GET },
            new object[] { "GET", RequestMethod.GET  },
             new object[] { "HEAD", RequestMethod.HEAD  },
              new object[] { "hEaD", RequestMethod.HEAD  },
               new object[] { "POST", RequestMethod.POST  },
                new object[] { "post", RequestMethod.POST  },
                 new object[] { "PUT", RequestMethod.PUT  },
                  new object[] { "DELETE", RequestMethod.DELETE  },
                   new object[] { "deleTe", RequestMethod.DELETE  },
                    new object[] { "connect", RequestMethod.CONNECT  },
                  new object[] { "OPtions", RequestMethod.OPTIONS  },
                  new object[] { "traCE", RequestMethod.TRACE  },
                  new object[] { "p3TH", RequestMethod.UNKNOWN  },
                  new object[] { "GETS", RequestMethod.UNKNOWN  },
                new object[] { "GETS", RequestMethod.UNKNOWN  },
                new object[] { "...", RequestMethod.UNKNOWN  },
            };
    }
}
