﻿using SimpleHTTPServer;
using SimpleHTTPServer.Models.Config;
using Xunit;

namespace HTTPServerTests
{
    public class ConfigParserTests
    {
        [Fact]
        public void Config_is_deserialized_correctly()
        {
            var testConfig = new string[] {"ip: 0.0.0.0" ,"port: 80", "defaultpath: " +
                "C:/test/test/test.conf", "routespath: C:/test/test/test.conf",
                "headersPath: C:/test/test/test.conf"};

            var deserializedConfig = ConfFileParser<ServerConfiguration>.DeSerialize(testConfig);

            Assert.Equal("0.0.0.0", deserializedConfig.Ip);
            Assert.Equal(80, deserializedConfig.Port);
            Assert.Equal("C:/test/test/test.conf", deserializedConfig.DefaultPath);
            Assert.Equal("C:/test/test/test.conf", deserializedConfig.RoutesPath);
            Assert.Equal("C:/test/test/test.conf", deserializedConfig.HeadersPath);
        }
    }
}
