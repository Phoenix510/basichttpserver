﻿using SimpleHTTPServer;
using System;
using Xunit;

namespace HTTPServerTests
{
    public class ConfSetterTests
    {
        //TODO still need to test reading settings from config file

        [Fact]
        public void Settings_are_read_from_args()
        {
            var args = new string[] {"-port", "80", "-ip", "192.168.1.10", "-routes", 
                "C://test//desktop//routes.conf", "-conf", "C://test//desktop//settings.conf",
                "-defPath", "//sites", "-headers", "C://test//desktop//headers.conf"};

            var config = ConfigurationSetter.SetupConfiguration(args);

            Assert.Equal("192.168.1.10", config.Ip.ToString()); 
            Assert.Equal(80, config.Port);
            Assert.Equal("//sites", config.DefaultPath);
            Assert.Equal("C://test//desktop//routes.conf", config.RoutesPath);
            Assert.Equal("C://test//desktop//headers.conf", config.HeadersPath);
        }

        [Fact]
        public void Invalid_IP_in_args_is_replaced_with_default_value()
        {
            var args = new string[] {"-ip", "invalidIP"};

            var conf = ConfigurationSetter.SetupConfiguration(args);

            Assert.Equal("127.0.0.1", conf.Ip.ToString());
            Assert.Equal(5500, conf.Port);
            Assert.Equal("/www", conf.DefaultPath);
            Assert.Equal(Environment.CurrentDirectory + @"/routes.conf", conf.RoutesPath);
            Assert.Equal(Environment.CurrentDirectory + @"/headers.conf", conf.HeadersPath);
        }

        [Fact]
        public void Invalid_port_in_args_is_replaced_with_default_value()
        {
            var args = new string[] { "-port", "invalidport" };

            var conf = ConfigurationSetter.SetupConfiguration(args);

            Assert.Equal("127.0.0.1", conf.Ip.ToString());
            Assert.Equal(5500, conf.Port);
            Assert.Equal("/www", conf.DefaultPath);
            Assert.Equal(Environment.CurrentDirectory + @"/routes.conf", conf.RoutesPath);
            Assert.Equal(Environment.CurrentDirectory + @"/headers.conf", conf.HeadersPath);
        }
    }
}
