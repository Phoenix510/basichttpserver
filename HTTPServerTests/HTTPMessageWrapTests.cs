﻿using SimpleHTTPServer;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace HTTPServerTests
{
    public class HTTPMessageWrapTests
    {
        [Fact]
        public void HTTP_Message_with_text_content_is_wrapped_correctly()
        {
            string body = "test body";
            var headers = new Dictionary<string, string>()
            {
                {"Content-Length", body.Length.ToString() },
                {"Test-Header", "test" }
            };

            var expectedMessageInText =
                Encoding.UTF8.GetBytes($"HTTP/1.1 200 OK{Environment.NewLine}" +
                $"Content-Length: {body.Length}{Environment.NewLine}" +
                $"Test-Header: test{Environment.NewLine}{Environment.NewLine}{body}"); 

            var wrappedMessage = HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.OK, headers, body);

            Assert.Equal(expectedMessageInText, wrappedMessage);
        }

        [Fact]
        public void HTTP_Message_with_binary_content_is_wrapped_correctly()
        {
            byte[] bytes = new byte[] { 0, 255, 2, 14, 0 };
            var headers = new Dictionary<string, string>()
            {
                {"Content-Length", bytes.Length.ToString() },
                {"Test-Header", "test" }
            };

            var expectedMessageInText =
                Encoding.UTF8.GetBytes($"HTTP/1.1 200 OK{Environment.NewLine}" +
                $"Content-Length: {bytes.Length}{Environment.NewLine}" +
                $"Test-Header: test{Environment.NewLine}{Environment.NewLine}").AddRange(bytes);

            var wrappedMessage = HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.OK, headers, bytes);

            Assert.Equal(expectedMessageInText, wrappedMessage);
        }
    }
}
