﻿using Xunit;
using SimpleHTTPServer;
using System.Collections.Generic;
using SimpleHTTPServer.Extensions;

namespace HTTPServerTests
{
    public class DictionaryExtensionsTests
    {
        [Theory]
        [MemberData(nameof(DictionaryData.NULLTestData), MemberType = typeof(DictionaryData))]
        public void Nulls_are_detected_correctly(Dictionary<int, int> dict1,
            Dictionary<int, int> dict2, object expectedResult)
        {
            var result = dict1.AddRange(dict2);
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Dictionary_are_combined_correctly()
        {
            var dict1 = new Dictionary<int, int>() { { 1, 1 }, { 2, 2 } };
            var dict2 = new Dictionary<int, int>() { { 3, 3 }, { 4, 4 } };
            var result = dict1.AddRange(dict2);

            var expectedResult = new Dictionary<int, int>() { { 1, 1 }, { 2, 2 }, 
                { 3, 3 }, { 4, 4 } };
            Assert.Equal(expectedResult, result);
        }
    }

    public class DictionaryData
    {
        public static IEnumerable<object[]> NULLTestData =>
            new List<object[]>
            {
            new object[] { new Dictionary<int, int>() { { 1, 1 }, { 2, 2 } },
                null, new Dictionary<int, int>() { { 1, 1 }, { 2, 2 } } },
            new object[] { null, null, null },
            new object[] { null, new Dictionary<int, int>() { { 1, 1 }, { 2, 2 } },
                new Dictionary<int, int>() { { 1, 1 }, { 2, 2 } }, },
            };
    }
}
