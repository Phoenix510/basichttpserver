﻿using Xunit;
using SimpleHTTPServer;

namespace HTTPServerTests
{
    public class ArrayExtensionsTests
    {
        [Fact]
        public void Int_arrays_are_combined_correctly()
        {
            var array1 = new int[] { 1, 2, 3, 4, 5, 6 };
            var array2 = new int[] { 7, 8, 9, 10, 11 };

            var result = array1.AddRange(array2);

            for(int i =0; i < 11; i++)
            {
                Assert.Equal(i + 1, result[i]);
            }
        }

        [Fact]
        public void String_arrays_are_combined_correctly()
        {
            var array1 = new string[] { "one", "two" };
            var array2 = new string[] { "three", "four", "five"};

            var result = array1.AddRange(array2);

            for (int i = 0; i < 5; i++)
            {
                Assert.Equal((i + 1).NumberToName(), result[i]);
            }
        }

        [Fact]
        public void Byte_arrays_are_combined_correctly()
        {
            var array1 = new byte[] { 1, 2 };
            var array2 = new byte[] { 3, 4, 5 };

            var result = array1.AddRange(array2);

            for (byte i = 0; i < 5; i++)
            {
                Assert.Equal(i + 1, result[i]);
            }
        }
    }

    public static class IntExtension
    {
        //
        // Summary:
        //     Converts int to string name e.g. 1 to one (works only with numbers 1-9) 
        //  
        //
        public static string NumberToName(this int number)
        {
            return number switch
            {
                1 => "one",
                2 => "two",
                3 => "three",
                4 => "four",
                5 => "five",
                6 => "six",
                7 => "seven",
                8 => "eight",
                9 => "nine",
                _ => "unknown",
            };
        }
    }
}
