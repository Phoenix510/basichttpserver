﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SimpleHTTPServer.Models
{
    internal interface HttpClient
    {
        byte[] ExpectationFailedResponse();
        byte[] ContinueResponse();
        Response CreateResponse(string path, HTTPData requestData, StatusCode code);
        Task<bool> HandleRequest(NetworkStream stream, HTTPData requestData, StatusCode code = StatusCode.OK, bool wasTheSameType = false);

        public static HttpClient CreateHttpClient(string protocol)
        {
            switch (protocol.ToUpperInvariant())
            {
                case "HTTP/1.1":
                    return new Http1Client();
                case "HTTP/2.0":
                    throw new NotImplementedException();
                default:
                    return new Http1Client();
            }
        }
    }
}
