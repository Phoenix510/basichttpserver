﻿using SimpleHTTPServer.Extensions;
using SimpleHTTPServer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleHTTPServer
{
    enum DataType
    {
        HTML,
        MP4,
        TEXT,
        AVI,
        BIN,
        CSS,
        DOC,
        GZ,
        ICO,
        MP3,
        MPEG,
        PNG,
        PDF,
        XML,
        ZIP,
        UNKNOWN
    }

    public interface Response
    {
        //public StatusCode StatusCode { get; set; }
        //public string ContentType { get; set; }
        //public BodyContent Content { get; set; }
        public byte[] ToBytes();
        public void AddHeader(string name, string value);


        public static string GetContentType(string type)
        {
            return type.ToLowerInvariant() switch
            {
                "html" => "text/html; charset=UTF-8",
                "mp4" => "video/mp4",
                "txt" => "text/plain",
                "avi" => "video/x-msvideo",
                "bin" => "application/octet-stream",
                "css" => "text/css",
                "doc" => "application/msword",
                "gz" => "application/gz",
                "ico" => "image/vnd.microsoft.icon",
                "mp3" => "audio/mpeg",
                "mpeg" => "video/mpeg",
                "png" => "image/png",
                "pdf" => "application/pdf",
                "xml" => "application/xml",
                "zip" => "application/zip",
                "svg" => "image/svg+xml",
                _ => "application/octet-stream",
            };
        }
    }
}
