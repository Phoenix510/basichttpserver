﻿using SimpleHTTPServer.Models;
using SimpleHTTPServer.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleHTTPServer
{
    //https://datatracker.ietf.org/doc/html/rfc2616#section-4
    public class HTTPData
    {
        //public string Method;
        public RequestMethod Method;
        public string Path;
        public string HTTPVersion;
        public Dictionary<string, string> Headers;
        public string RawData;
        public string Body = string.Empty;

        public HTTPData(string rawData)
        {
            Headers = new Dictionary<string, string>();
            if(!CheckData(rawData)) return;
            var splittedRawData = rawData.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            splittedRawData = GetTypeAndVersion(splittedRawData);
            Headers = GetHeaders(splittedRawData);
        }

        private bool CheckData(string rawData)
        {
            RawData = rawData;
            if (rawData is null)
            {
                Headers = new Dictionary<string, string>();
                Path = "/notFound";
                HTTPVersion = "HTTP/1.1";
                Method = RequestMethod.GET;
                return false;
            }
            return true;
        }

        private string[] GetTypeAndVersion(string[] rawData)
        {
            var splittedData = rawData[0].Split(" ");
            if (splittedData.Count() < 3)
            {
                Path = "/notFound";
                HTTPVersion = "HTTP/1.1";
                Method = RequestMethod.GET;
                return new string[0];
            }

            Method = splittedData[0].ToRequestMethod();
            Path = splittedData[1].Split('?')[0];
            if (Method == RequestMethod.UNKNOWN || !Validator.ValidatePath(Path))
                Path = "/notFound";

            HTTPVersion = splittedData[2];

            return rawData.Skip(1).ToArray();
        }

        private Dictionary<string, string> GetHeaders(string[] rawData)
        {
            var headers = new Dictionary<string, string>();

            for (int i = 0; i < rawData.Length - 1; i++)
            {
                var splitedHeader = rawData[i].Split(new[] { ": " }, StringSplitOptions.None);
                if (splitedHeader.Count() > 1)
                    headers.Add(splitedHeader[0], splitedHeader[1]);
                else
                    headers.Add(splitedHeader[0], "");
            }

            return headers;
        }
    }
}
