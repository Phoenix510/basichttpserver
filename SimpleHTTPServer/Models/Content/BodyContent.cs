﻿using SimpleHTTPServer.Models.Content;
using System.Collections.Generic;
using System.IO;

namespace SimpleHTTPServer.Models
{
    public interface BodyContent
    {
        public byte[] CreateResponseAsBytes(bool shouldHasBody, StatusCode code, 
            Dictionary<string, string> defaultHeaders);

        public static BodyContent CreateBodyContent(string path, string contentType, 
            Dictionary<string, string> headers)
        {
            if (string.IsNullOrEmpty(path) || !File.Exists(path))
            {
                return new EmptyContent(EmptyReason.FileNotFound);
            }
            var requestedFileLength = new FileInfo(path).Length;
            var rangeHeaderFound = headers.TryGetValue("Range", out string range);

            if(requestedFileLength > 999999 && !rangeHeaderFound)
                return new EmptyContent(EmptyReason.PayloadTooLarge);
            else if (contentType.StartsWith("text"))
                return new TextContent(path, contentType);
            else if (!rangeHeaderFound)
                return new BinaryContent(path, contentType);
            else
                return CreatePartialBinaryContent(path, range, requestedFileLength, contentType);
        }

        private static PartialBinaryContent CreatePartialBinaryContent
            (string path, string range, long fileLength, string contentType)
        {
            var indexes = range.Split('-');
            indexes[0] = indexes[0].Replace("bytes=", "");
            if (string.IsNullOrWhiteSpace(indexes[1]))
                indexes[1] = (int.Parse(indexes[0]) + 999999).ToString();

            if (int.Parse(indexes[1]) > fileLength)
                indexes[1] = (fileLength - 1).ToString();

            return new PartialBinaryContent(path, int.Parse(indexes[0]),
                int.Parse(indexes[1]) - int.Parse(indexes[0]) + 1, fileLength, contentType);
        }
    }
}
