﻿using SimpleHTTPServer.Extensions;
using System.Collections.Generic;
using System.IO;

namespace SimpleHTTPServer.Models
{
    public class TextContent : BodyContent
    {
        private string content = string.Empty;
        private string contentType = string.Empty;

        public TextContent(string path, string contentType)
        {
            content = (string)ReadContent(path);
            this.contentType = contentType;
        }

        public virtual byte[] CreateResponseAsBytes(bool shouldHasBody, StatusCode code, Dictionary<string, string> defaultHeaders)
        {
            var headers = new Dictionary<string, string>
            {
                { "Content-Type", contentType },
                { "Content-Length", content.Length.ToString() }
            };
            headers.AddRange(defaultHeaders);

            if (shouldHasBody)
                return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", code, headers, content);
            else
                return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", code, headers, string.Empty);
        }

        private object ReadContent(string path)
        {
            return File.ReadAllText(path);
        }
    }
}
