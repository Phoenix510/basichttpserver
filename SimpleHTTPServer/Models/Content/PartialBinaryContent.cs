﻿using SimpleHTTPServer.Extensions;
using System.Collections.Generic;
using System.IO;

namespace SimpleHTTPServer.Models.Content
{
    public class PartialBinaryContent : BodyContent
    {
        public int Offset { get; }
        public int NumberOfBytes { get; }
        public long FileLength { get; }
        private byte[] content = new byte[0];
        private string contentType = string.Empty;

        public PartialBinaryContent(string path, int offset, int numberOfBytes, long fileLength, string contentType)
        {
            Offset = offset;
            NumberOfBytes = numberOfBytes;
            FileLength = fileLength;
            content = (byte[])ReadContent(path, offset, numberOfBytes);
            this.contentType = contentType;
        }

        private object ReadContent(string path, int offset, int bytesToRead)
        {
            byte[] buffer = new byte[bytesToRead];

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                fs.Seek(offset, SeekOrigin.Begin);
                fs.Read(buffer, 0, bytesToRead);
            }

            return buffer;
        }

        public byte[] CreateResponseAsBytes(bool shouldHasBody, StatusCode code, Dictionary<string, string> defaultHeaders)
        {
            var headers = new Dictionary<string, string>
            {
                { "Content-Type", contentType },
                { "Content-Length", content.Length.ToString() },
                { "Accept-Range", "bytes" },
                { "Content-Range", $"bytes {Offset}-{Offset + NumberOfBytes -1}/{FileLength}" }
            };
            headers.AddRange(defaultHeaders);

            if (shouldHasBody)
                return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.PartialContent, headers, content);
            else
                return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", code, headers, new byte[0]);
        }
    }
}
