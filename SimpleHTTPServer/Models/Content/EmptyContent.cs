﻿using SimpleHTTPServer.Extensions;
using System;
using System.Collections.Generic;

namespace SimpleHTTPServer.Models.Content
{
    public enum EmptyReason
    {
        PayloadTooLarge,
        FileNotFound,
        Unknown
    }

    public class EmptyContent : BodyContent
    {
        private EmptyReason WhyEmpty = EmptyReason.Unknown;

        public EmptyContent(EmptyReason reason)
        {
            WhyEmpty = reason;
        }

        public byte[] CreateResponseAsBytes(bool shouldHasBody, StatusCode code, Dictionary<string, string> defaultHeaders)
        {
            var headers = new Dictionary<string, string>
            {
                { "Content-Length", "0" },
                { "Accept-Range", "bytes" },
            };
            headers.AddRange(defaultHeaders);

            switch (WhyEmpty)
            {
                case EmptyReason.PayloadTooLarge:
                    code = StatusCode.PayloadTooLarge;
                    break;
                case EmptyReason.FileNotFound:
                    code = StatusCode.NotFound;
                    break;
                default:
                    code = StatusCode.BadRequest;
                    break;
            }

            return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", code, headers, string.Empty);
        }
    }
}
