﻿using SimpleHTTPServer.Extensions;
using System.Collections.Generic;
using System.IO;

namespace SimpleHTTPServer.Models.Content
{
    public class BinaryContent: BodyContent
    {
        private byte[] content = new byte[0];
        private string contentType = string.Empty;

        public BinaryContent(string path, string contentType)
        {
            content = (byte[])ReadContent(path);
            this.contentType = contentType;
        }

        public byte[] CreateResponseAsBytes(bool shouldHasBody, StatusCode code, Dictionary<string, string> defaultHeaders)
        {
            var headers = new Dictionary<string, string>
            {
                { "Content-Type", contentType },
                { "Content-Length", content.Length.ToString() },
                { "Accept-Range", "bytes" }
            };
            headers.AddRange(defaultHeaders);

            if (shouldHasBody)
                return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", code, headers, content);
            else
                return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", code, headers, new byte[0]);
        }

        private object ReadContent(string path)
        {
            return File.ReadAllBytes(path);
        }
    }
}
