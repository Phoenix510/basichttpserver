﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SimpleHTTPServer
{
    public class HttpLog
    {
        public readonly string Method;
        public readonly string Path;
        public readonly string HTTPVersion;
       // public readonly Dictionary<string, string> Headers;
        public readonly string IP;

        public HttpLog(HTTPData dataToLog, System.Net.IPAddress ip)
        {
            Method = dataToLog.Method.ToString("g");
            Path = dataToLog.Path;
            HTTPVersion = dataToLog.HTTPVersion;
           // Headers = dataToLog.Headers;
            IP = ip.ToString();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
