﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleHTTPServer
{
    public class BodyReader
    {
        public string BodyAsString = string.Empty;
        private int dataLength = 0;
        private int expectedLength = 0;

        public BodyReader(int expectedLength)
        {
            this.expectedLength = expectedLength;
        }

        public bool ReadData(byte[] dataBytes, int count)
        {
            dataLength += count;
            var data = Encoding.UTF8.GetString(dataBytes, 0, count);
            BodyAsString += data;

            if(dataLength >= expectedLength)
                return true;

            return false;
        }
    }
}
