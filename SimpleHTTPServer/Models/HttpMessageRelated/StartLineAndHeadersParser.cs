﻿using System.Text;

namespace SimpleHTTPServer
{
    public class StartLineAndHeadersReader
    {
        public string StartLineAndHeadersAsString = string.Empty;
        private int dataLength = 0;

        public (bool partEnded, bool isTooBig) ReadData(byte[] dataBytes, int count)
        {
            dataLength += count;
            if (dataLength > 16845)
            {
                return (false, true);
            }
            var data = Encoding.UTF8.GetString(dataBytes, 0, count);
            StartLineAndHeadersAsString += data;

            if (data.Contains("\r\n\r\n") || data.Contains("\n\n"))
            {
                return (true, false);
            }

            return (false, false);
        }

        public (HTTPData data, int expectedLength) ParseData()
        {
            var httpData = new HTTPData(StartLineAndHeadersAsString);
            if (!httpData.Headers.TryGetValue("Content-Length",
                out string expectedLengthString)
            || !int.TryParse(expectedLengthString, out int expectedLength))
                return (httpData, 0);
            else
            {
                if (expectedLength <= 0)
                    return (httpData, 0);
                return (httpData, expectedLength);
            }
        }

    }
}
