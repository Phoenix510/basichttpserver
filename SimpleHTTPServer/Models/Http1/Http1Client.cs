﻿using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SimpleHTTPServer.Models
{
    public class Http1Client : HttpClient
    {
        public byte[] ExpectationFailedResponse()
        {
            return Encoding.UTF8.GetBytes("HTTP/1.1 417 Expectation Failed\r\n\r\n");
        }

        public byte[] ContinueResponse()
        {
            return Encoding.UTF8.GetBytes("HTTP/1.1 100 Continue\r\n\r\n");
        }

        public byte[] SwitchingProtocolsResponse()
        {
            return Encoding.UTF8.GetBytes("HTTP/1.1 101 Switching Protocols\r\nConnection:Upgrade\r\nUpgrade:h2c\r\n\r\n");
        }

        public Response CreateResponse(string path, HTTPData requestData, StatusCode code)
        {
            switch (requestData.Method)
            {
                case RequestMethod.GET:
                    return new ResponseHTTP1GetHead(path, requestData, code, true);
                case RequestMethod.OPTIONS:
                    var resp = new ResponseHTTP1Options(path, requestData);
                    return resp;
                case RequestMethod.HEAD:
                    return new ResponseHTTP1GetHead(path, requestData, code, false);
                default:
                    return new ResponseHTTP1GetHead(path, requestData, code, true);
            }
        }

        public async Task<bool> HandleRequest(NetworkStream stream, HTTPData requestData, StatusCode code = StatusCode.OK,
            bool wasTheSameType = false)
        {
            var newClient = UpgradeConnHandler.UpgradeConnectionIfNecessary(requestData);
            if (newClient is not null && !wasTheSameType)
            {
                await newClient.HandleRequest(stream, requestData, wasTheSameType: true);
                return true;
            }

            string path = requestData.Path;
            if (Program.Configuration.CustomRoutes.TryGetValue(path, out string newPath))
                path = newPath;

            if (!File.Exists(Program.Configuration.DefaultPath + path))
            {
                if (!Program.Configuration.CustomRoutes.TryGetValue("/notFound", out path) || !File.Exists(Program.Configuration.DefaultPath + path))
                {
                    var resp = CreateResponse(string.Empty, requestData, StatusCode.NotFound).ToBytes();
                    await stream.WriteAsync(resp, 0, resp.Length);
                    return true;
                }

                code = StatusCode.NotFound;
            }

            var response = CreateResponse(Program.Configuration.DefaultPath + path, requestData, code);
            if (ShouldConnectionStayOpen(requestData.Headers))
            {
                response.AddHeader("Connection", "Keep-Alive");
                response.AddHeader("Keep-Alive", "timeout=10, max=1000");
                SendResponse(response, stream);
                return true;
            }     

            SendResponse(response, stream);

            return false;
        }

        private async void SendResponse(Response response, NetworkStream stream)
        {
            var respAsBytes = response.ToBytes();
            await stream.WriteAsync(respAsBytes, 0, respAsBytes.Length);
        }

        private bool ShouldConnectionStayOpen(Dictionary<string, string> headers)
        {
            if (!headers.TryGetValue("Connection", out string connection))
                return false;

            if (connection.ToLowerInvariant() != "keep-alive")
                return false;

            return true;
        }
    }
}
