﻿using System.Collections.Generic;

namespace SimpleHTTPServer.Models
{
    internal class ResponseHTTP1Options : Response
    {
        private readonly HTTPData requestData;
        private readonly string path;
        private Dictionary<string, string> headers = new Dictionary<string, string>();

        public ResponseHTTP1Options(string path, HTTPData reqData)
        {
            requestData = reqData;         
            this.path = path;
        }

        public void AddHeader(string name, string value)
        {
            headers.Add(name, value);
        }

        public byte[] ToBytes()
        {
            //here should be checked which methods are available for specified file
            AddHeader("Allow", "GET, OPTIONS, HEAD");
            return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.NoContent, headers, string.Empty);
        }
    }
}
