﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleHTTPServer.Models
{
    public class ResponseHTTP1GetHead: Response
    {
        public StatusCode StatusCode { get; set; }
        public string ContentType { get; set; }
        public BodyContent Content { get; set; }

        private readonly HTTPData requestData;
        private Dictionary<string, string> headers = new Dictionary<string, string>();
        private bool shouldHasBody = true;

        public ResponseHTTP1GetHead(string path, HTTPData reqData, StatusCode statusCode, bool shouldHasBody = true)
        {
            ContentType = Response.GetContentType(path.Split('.').Last());
            StatusCode = statusCode;
            requestData = reqData;
            Content = BodyContent.CreateBodyContent(path, ContentType, requestData.Headers);
            this.shouldHasBody = shouldHasBody;
            if (reqData.Path == "/notFound")
                StatusCode = StatusCode.NotFound;
        }

        public void AddHeader(string name, string value)
        {
            headers.Add(name, value);
        }

        public byte[] ToBytes()
        {
            if (Content is null)
            {
                return HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.BadRequest, headers, string.Empty);
            }

            return Content.CreateResponseAsBytes(shouldHasBody, StatusCode, this.headers);
        }
    }
}
