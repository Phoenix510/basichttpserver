﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SimpleHTTPServer.Models.Config
{
    public class ServerConfiguration
    {
        public string Ip = "127.0.0.1";
        public int Port = 5500;
        public string DefaultPath = "/www";
        public string RoutesPath = Environment.CurrentDirectory + @"/routes.conf";
        public string HeadersPath = Environment.CurrentDirectory + @"/headers.conf";
        public Dictionary<string, string> DefaultHeaders = new Dictionary<string, string>();
        public Dictionary<string, string> CustomRoutes = new Dictionary<string, string>();

        public ServerConfiguration() { }

        public void SetupRoutes(string pathToFile)
        {
            if (!File.Exists(pathToFile))
                return;

            var routesToSplt = File.ReadAllLines(pathToFile);
            foreach (var line in routesToSplt)
            {
                var splitted = line.Split(new[] { ": " }, StringSplitOptions.None);
                if (splitted.Length < 2)
                    continue;

                CustomRoutes.Add(splitted[0], splitted[1]);
            }
        }

    }
}
