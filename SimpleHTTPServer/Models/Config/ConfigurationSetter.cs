﻿using SimpleHTTPServer.Models.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace SimpleHTTPServer
{
    public static class ConfigurationSetter
    {
        public static ServerConfiguration SetupConfiguration(string[] args)
        {
            var assignedArgs = AssignArgsToNames(args);
            var confPath = assignedArgs.TryGetValue("conf", out string conf) ? conf : Environment.CurrentDirectory + @"/mainSettings.conf";
            var configuration = ReadAndSetupConfig(confPath);
            SetupConfigurationFromArgs(assignedArgs, configuration);
            ReadAndSetupDefaultHeaders(configuration);

            configuration.DefaultPath = Directory.GetCurrentDirectory() + configuration.DefaultPath;
            if (string.IsNullOrEmpty(configuration.RoutesPath))
                configuration.SetupRoutes(Directory.GetCurrentDirectory() + "/routes.conf");
            else
                configuration.SetupRoutes(configuration.RoutesPath);
            return configuration;
        }

        private static void ReadAndSetupDefaultHeaders(ServerConfiguration configuration)
        {
            if (!File.Exists(configuration.HeadersPath))
                return;
            var headers = File.ReadAllLines(configuration.HeadersPath);

            foreach(var line in headers)
            {
                var splitted = line.Split(new[] { ": " }, StringSplitOptions.None);
                if (splitted.Length < 2 || string.IsNullOrWhiteSpace(splitted[0]) 
                    || string.IsNullOrWhiteSpace(splitted[1]))
                    return;

                configuration.DefaultHeaders.Add(splitted[0], splitted[1]);
            }
        }

        private static void SetupConfigurationFromArgs(Dictionary<string, string> assignedArgs, ServerConfiguration configuration)
        {
            configuration.Ip =  assignedArgs.TryGetValue("ip", out string ip) ? (IPAddress.TryParse(ip, out IPAddress iPAddress) ?ip : configuration.Ip) : configuration.Ip;
            configuration.Port = assignedArgs.TryGetValue("port", out string fPort) ? 
                (int.TryParse(fPort, out int convPort) ? convPort : configuration.Port) : configuration.Port;
            configuration.RoutesPath = assignedArgs.TryGetValue("routes", out string routes) ? routes : configuration.RoutesPath;
            configuration.HeadersPath = assignedArgs.TryGetValue("headers", out string headers) ? headers : configuration.HeadersPath;
            configuration.DefaultPath = assignedArgs.TryGetValue("defPath", out string defPath) ? defPath : configuration.DefaultPath;
        }

        private static Dictionary<string, string> AssignArgsToNames(string[] args)
        {
            var assignedArgs = new Dictionary<string, string>();
            int i = 0;
            while (i < args.Length - 1)
            {
                if (args[i].StartsWith('-'))
                {
                    if (args[i + 1].StartsWith('-'))
                        assignedArgs.Add(args[i].Remove(0, 1), "");
                    else
                        assignedArgs.Add(args[i].Remove(0, 1), args[i + 1]);
                }
                i++;
            }
            return assignedArgs;
        }

        private static ServerConfiguration ReadAndSetupConfig(string confPath)
        {
            if (string.IsNullOrEmpty(confPath) || !File.Exists(confPath))
                return new ServerConfiguration();

            return ConfFileParser<ServerConfiguration>.DeSerialize(File.ReadAllLines(confPath));

        }
    }
}
