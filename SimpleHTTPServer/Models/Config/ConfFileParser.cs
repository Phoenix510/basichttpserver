﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SimpleHTTPServer
{
    public class ConfFileParser<T> where T : new()
    {
        public static T DeSerialize(string[] toSeriazlize)
        {
            var assignedValues = getValues(toSeriazlize);
            T newObject = new T();

            foreach (var x in typeof(T).GetFields())
            {
                foreach(var value in assignedValues)
                {
                    if (value.Key.ToLowerInvariant() == x.Name.ToLowerInvariant())
                        newObject = setValue(newObject, x.Name, value.Value);
                }
            }

            return newObject;
        }

        private static T setValue(T obj, string fieldName, string value)
        {
            Type type = obj.GetType();
            FieldInfo field = type.GetField(fieldName);
            object fieldValue = value;

            if (field.FieldType != typeof(string))
            {
                TypeConverter typeConverter = TypeDescriptor.GetConverter(field.FieldType);
                fieldValue = typeConverter.ConvertFromString(value);
            }

            field.SetValue(obj, fieldValue);
            return obj;
        }


        private static Dictionary<string, string> getValues(string[] content)
        {
           var assignedValues = new Dictionary<string, string>();
            foreach (var line in content)
            {
                var splitted = line.Split(new[] { ": " }, StringSplitOptions.None);
                if (splitted.Length < 2 || string.IsNullOrWhiteSpace(splitted[0])
                    || string.IsNullOrWhiteSpace(splitted[1]))
                    continue;

                assignedValues.Add(splitted[0], splitted[1]);
            }

            return assignedValues;
        }

    }
}
