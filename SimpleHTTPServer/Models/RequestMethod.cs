﻿namespace SimpleHTTPServer.Models
{
    public enum RequestMethod
    {
        GET,
        POST,
        PUT,
        DELETE,
        OPTIONS,
        HEAD,
        TRACE,
        CONNECT,
        UNKNOWN
    }

    public static class StringExtension
    {
        public static RequestMethod ToRequestMethod(this string methodAsString)
        {
            return methodAsString.ToLowerInvariant() switch
            {
                "get" => RequestMethod.GET,
                "post" => RequestMethod.POST,
                "put" => RequestMethod.PUT,
                "delete" => RequestMethod.DELETE,
                "options" => RequestMethod.OPTIONS,
                "head" => RequestMethod.HEAD,
                "trace" => RequestMethod.TRACE,
                "connect" => RequestMethod.CONNECT,
                "unknown" => RequestMethod.UNKNOWN,
                _ => RequestMethod.UNKNOWN,
            };
        }
    }
}
