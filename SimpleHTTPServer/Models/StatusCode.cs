﻿namespace SimpleHTTPServer
{
    public enum StatusCode
    {
        OK = 200,
        Created = 201,
        NotFound = 404,
        PartialContent = 206,
        BadRequest = 400,
        Unauthorized = 401,
        SwitchingProtocols = 101,
        PayloadTooLarge = 413,
        RangeNotSatisfiable = 416,
        NoContent = 204
    }

    public static class StatusCodeExtension
    {
        public static string GetName(this StatusCode code)
        {
            return code switch
            {
                StatusCode.OK => "200 OK",
                StatusCode.Created => "201 Created",
                StatusCode.NotFound => "404 Not Found",
                StatusCode.PartialContent => "206 Partial Content",
                StatusCode.BadRequest => "400 Bad Request",
                StatusCode.Unauthorized => "401 Unauthorized",
                StatusCode.SwitchingProtocols => "101 Switching Protocols",
                StatusCode.PayloadTooLarge => "413 Payload Too Large",
                StatusCode.RangeNotSatisfiable => "416 Range Not Satisfiable",
                StatusCode.NoContent => "204 No Content",
                _ => "400 Bad Request",
            };
        }
    }
}
