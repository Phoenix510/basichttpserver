﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleHTTPServer
{
    //based on: https://docs.microsoft.com/pl-pl/dotnet/api/system.net.sockets.tcplistener?view=net-6.0
    //rfc 2616 https://datatracker.ietf.org/doc/html/rfc2616
    //and https://developer.mozilla.org

    public class TCPServer
    {
        readonly TcpListener server = null;
        private bool isRunning = false;

        public TCPServer(IPAddress ip, int port)
        {
            // this.port = port;
            // this.ipAddress = ip;
            server = new TcpListener(ip, port);
        }

        public void Start()
        {
            if (isRunning)
                return;

            server.Start();
            isRunning = true;
            StartListening();
        }

        public void Stop()
        {
            server.Stop();
            isRunning = false;
        }

        private void StartListening()
        {
            int i = 0;
            while (isRunning)
            {
                TcpClient client = server.AcceptTcpClient();
                client.SendTimeout = 0;
                i++;
                Console.WriteLine($"NEWCLIENT + {i}");

                HandleConnection(client);

            }
        }

        private void HandleConnection(TcpClient client)
        {
            try
            {
                NetworkStream stream = client.GetStream();
                new ConnectionHandler(client).HandleConnection(stream);
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

    }
}
