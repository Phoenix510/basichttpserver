﻿using System;

namespace SimpleHTTPServer
{
    public static class ArrayExtension
    {
        public static T[] AddRange<T>(this T[] array, T[] toCombine)
        {
            var combinedArray = new T[array.Length + toCombine.Length];
            Array.Copy(array, combinedArray, array.Length);
            Array.Copy(toCombine, 0, combinedArray, array.Length, toCombine.Length);

            return combinedArray;
        }
    }
}
