﻿using System.Collections.Generic;

namespace SimpleHTTPServer.Extensions
{
    public static class DictionaryExtension
    {
        public static Dictionary<K, V> AddRange<K, V>(this Dictionary<K, V> dict, Dictionary<K, V> toCombine)
        {
            if (dict is null && toCombine is null)
                return null;
            else if (toCombine is null)
                return dict;
            else if (dict is null)
                return toCombine;

            foreach (var header in toCombine)
            {
                dict.Add(header.Key, header.Value);
            }

            return dict;
        }
    }
}
