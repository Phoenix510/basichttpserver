﻿using SimpleHTTPServer.Models;
using System;
using System.Buffers.Text;
using System.Text;

namespace SimpleHTTPServer
{
    class UpgradeConnHandler
    {
        public static HttpClient UpgradeConnectionIfNecessary(HTTPData requestData)
        {
            if (!requestData.Headers.TryGetValue("Upgrade", out string upgradeHeader))
                return null;

            switch (upgradeHeader.ToLowerInvariant())
            {
                case "h2c":
                    //atm server does not supports http2 (either h2c or h2)
                    return new Http1Client();
                    //var response = new ResponseHTTP1(path, requestData, StatusCode.SwitchingProtocols);
                    //response.AddHeader("Connection", "Upgrade");
                    //response.AddHeader("Upgrade", "h2c");
                    //return response;
                    //return UpgradeToHTTP2(requestData);
                case "h2": // notimplementedyet
                    break;
                case "ws": //notimplementedyet
                    break;
            }

            return new Http1Client(); 
        }
    }
}
