﻿using SimpleHTTPServer.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleHTTPServer
{
    public static class HTTPWrapper 
    {
        public static byte[] CreateHTTPMessage(string protocolVersion, StatusCode statusCode, Dictionary<string, string> headers, string body)
        {
            string message = $"{protocolVersion} {statusCode.GetName()}{Environment.NewLine}";
            headers.AddRange(Program.Configuration.DefaultHeaders);
            foreach(var header in headers)
            {
                message += $"{header.Key}: {header.Value}{Environment.NewLine}";
            }

            message += $"{Environment.NewLine}{body}";
            return Encoding.UTF8.GetBytes(message);
        }

        public static byte[] CreateHTTPMessage(string protocolVersion, StatusCode statusCode, Dictionary<string, string> headers, byte[] body)
        {
            string message = $"{protocolVersion} {statusCode.GetName()}{Environment.NewLine}";
            headers.AddRange(Program.Configuration.DefaultHeaders);
            foreach (var header in headers)
            {
                message += $"{header.Key}: {header.Value}{Environment.NewLine}";
            }
            message += Environment.NewLine;
            var messageAsBytes = Encoding.UTF8.GetBytes(message);
         
            return messageAsBytes.AddRange(body);
        }
    }
}
