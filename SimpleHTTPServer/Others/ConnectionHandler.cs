﻿using Serilog;
using SimpleHTTPServer.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleHTTPServer
{
    class ConnectionHandler
    {
        private readonly TcpClient client;
        private HttpClient httpClient;
        private readonly Guid id;
        private NetworkStream stream;
        private StartLineAndHeadersReader startLineAndHeaders = new StartLineAndHeadersReader();
        private HTTPData httpData = null;
        private BodyReader body = null;
        private int expectedBodyLength = 0;
        private bool keepAlive = true;

        public ConnectionHandler(TcpClient client)
        {
            this.client = client;
            id = Guid.NewGuid();
        }

        public void HandleConnection(NetworkStream stream)
        {
            new Thread(() =>
            {
                HandleStream(stream);
            }).Start();
        }

        int noOfRequests = 0;
        private void HandleStream(NetworkStream stream)
        {
            this.stream = stream;       
            try
            {
                //loop for multiple request within one tcp connection
                //(max 10s between them and 1000 request in one conn)
                while (keepAlive)
                {
                    httpData = null;
                    body = null;
                    expectedBodyLength = 0;
                    startLineAndHeaders = new StartLineAndHeadersReader();

                    Task<bool> task = Task.Run(async Task<bool>() =>
                    {
                        return await HandleRequest(stream);
                    });

                    //10s timeout
                    if (task.Wait(TimeSpan.FromSeconds(10)))
                    {
                        if (task.Result)
                            continue;

                        client.Close();
                        Dispose();
                        keepAlive = false;
                    }
                    else
                    {                     
                        client.Close();
                        Dispose();
                        keepAlive = false;
                    }
                }
            }
            catch (Exception ex) { Debug.WriteLine(ex.Message); Log.Error($"Connection: {id} Req:{noOfRequests} {ex.Message}\n{ex.InnerException}"); }
        }

        //sends a response to a client and
        //returns a bool value which indicates whether a tcp connection should stay open
        private async Task<bool> HandleRequest(NetworkStream stream)
        {
            byte[] buffer = new byte[1024];
            int i;
            while ((i = stream.Read(buffer, 0, buffer.Length)) >= 0)
            {
                if (httpData == null)
                {
                    var readingFinished = ReadStartLineAndHeaders(buffer, i);
                    if (readingFinished && httpData is null)
                        return false;
                    if (readingFinished && expectedBodyLength == 0)
                        break;
                    body = new BodyReader(expectedBodyLength);
                    continue;
                }

                var bodyRead = body.ReadData(buffer, i);
                if (!bodyRead)
                    continue;

                httpData.Body = body.BodyAsString;
                break;
            }

           // Debug.WriteLine(httpData.RawData);
            Log.Information($"Connection: {id} Req:{noOfRequests} " + new HttpLog(httpData,
                            ((IPEndPoint)client.Client.RemoteEndPoint).Address).ToString());

            noOfRequests++;
            var doKeepAlive = await httpClient.HandleRequest(stream, httpData);
            if (!doKeepAlive || noOfRequests >= 1000)
            {
                return false;
            }
            return true;
        }

        private bool ReadStartLineAndHeaders(byte[] buffer, int i)
        {
            (bool partEnded, bool isTooBig) result = startLineAndHeaders.ReadData(buffer, i);
            if (result.isTooBig)
            {
                TooBigEntity();
                return true;
            }
            else if (result.partEnded)
            {
                (HTTPData data, int expectedLength) parsingResult = startLineAndHeaders.ParseData();
                httpData = parsingResult.data;
                httpClient = HttpClient.CreateHttpClient(httpData.HTTPVersion);

                if (parsingResult.expectedLength > 16845)
                {
                    var tooBigResponse = httpClient.ExpectationFailedResponse();
                    stream.Write(tooBigResponse, 0, tooBigResponse.Length);
                    client.Close();
                    Dispose();
                    return true;
                }
                if (httpData.Headers.TryGetValue("Expect", out string hValue) && hValue.ToLowerInvariant() == "100-continue")
                {
                    var resp = httpClient.ContinueResponse();
                    stream.Write(resp, 0, resp.Length);
                }
                expectedBodyLength = parsingResult.expectedLength;
                return true;
            }
            return false;
        }

        private void TooBigEntity()
        {
            var tooBigResponse = HTTPWrapper.CreateHTTPMessage("HTTP/1.1", StatusCode.PayloadTooLarge,
                new Dictionary<string, string>(), string.Empty);
            stream.Write(tooBigResponse, 0, tooBigResponse.Length);
            client.Close();
            Dispose();
        }

        private void Dispose([CallerMemberName] string name = "")
        {
            client.Dispose();
            if (stream is not null)
                stream.Dispose();
        }
    }
}
