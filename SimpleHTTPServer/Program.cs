﻿using Serilog;
using SimpleHTTPServer.Models.Config;
using System;
using System.Net;

namespace SimpleHTTPServer
{
    class Program
    {
        public static ServerConfiguration Configuration { get; set; } = new ServerConfiguration();
        static TCPServer server = null;
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Information()
               .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Warning)
               .Enrich.FromLogContext()
               .WriteTo.Console()
               .WriteTo.File(Environment.CurrentDirectory + @"/logs/logs.txt")
               .CreateLogger();

            Configuration = ConfigurationSetter.SetupConfiguration(args);
            Log.Information($"Started on port: {Configuration.Port}, ip: {Configuration.Ip}");
            var ip = IPAddress.TryParse(Configuration.Ip, out IPAddress ipParsed) ? ipParsed : IPAddress.Parse("127.0.0.1");

            server = new TCPServer(ip, Configuration.Port);
            server.Start();
            server.Stop();
        }     
    }
}
